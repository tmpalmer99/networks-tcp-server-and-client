import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

class ServerProtocol {

    private static final int WAITING = 1;
    private static final int LIST = 2;
    private static final int GET = 3;
    private static final int PUT = 4;
    private static final int REQUESTHANDLED = 5;

    private int state = WAITING;

    public String processInput(String theInput) {
        StringBuilder theOutput = new StringBuilder();
        if(state == WAITING) {
            switch (theInput) {
                case "list":
                    state = LIST;
                    theOutput = new StringBuilder("Listing Server Files...\n");
                    theOutput.append("------------");
                    File path = new File("./serverFiles/");
                    File[] files = path.listFiles();
                    assert files != null;
                    for (File file : files) {
                        theOutput.append("\n").append(file.getName());
                    }
                    theOutput.append("\n------------");
                    state = REQUESTHANDLED;
                    break;
                case "get":
                    state = GET;
                    theOutput = new StringBuilder("What file is client requesting?");
                    break;
                case "put":
                    state = PUT;
                    theOutput = new StringBuilder("getting file data...");
                    break;
            }
        }
        else if(state == GET) {
            try {
                theOutput = new StringBuilder("Sending File...\n");
//                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("./serverFiles/" + theInput)));
//                String fileLine = bufferedReader.readLine();
//                theOutput.append(fileLine);
//                while ((fileLine = bufferedReader.readLine()) != null) {
//                    theOutput.append("\n");
//                    theOutput.append(fileLine);
//                }
                byte[] byteArray = Files.readAllBytes(Paths.get("./serverFiles/" + theInput));
                theOutput.append(new String(byteArray, StandardCharsets.ISO_8859_1));
                System.out.println(theOutput.toString());
                state = REQUESTHANDLED;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(state == REQUESTHANDLED) {
            theOutput = new StringBuilder("Bye.");
            state = WAITING;
        }
        return theOutput.toString();
    }
}
