import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

	private static final int portNumber = 8888;

	private void ServerExecutor() throws IOException {
		ServerSocket serverSocket = null;
		ExecutorService executorService;

		// Try to set up server socket to listen on port 8888
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			System.err.println("Could not listen on port: " + portNumber);
			System.exit(-1);
		}

		System.out.print("\033[H\033[2J");
		System.out.println("Server listening on port " + portNumber + "...");

		// Wait for a client to make contact
		executorService = Executors.newFixedThreadPool(10);

		while(true) {
			// For each new client, submit a new handler to the thread pool.
			Socket clientSocket = serverSocket.accept();
			executorService.submit( new ClientHandler(clientSocket));
		}
	}

	public static void main( String[] args ) {
		Server server = new Server();
		try {
			server.ServerExecutor();
		} catch(IOException e) {
			System.out.print(e);
		}


	}
}