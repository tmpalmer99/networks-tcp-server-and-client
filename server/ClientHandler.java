import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

//
//            InetAddress inetAddress = clientSocket.getInetAddress();
//
//            // Create log entry for the new client
//            BufferedWriter bufferedWriter = new BufferedWriter(
//                    new FileWriter(
//                            new File("log.txt"), true));
//
//            String timeStamp = new SimpleDateFormat("dd/MM/yyyy:HH.mm.ss").format(Calendar.getInstance().getTime());
//            bufferedWriter.write(timeStamp + ":" + inetAddress.getHostAddress() + ":" + serverInput);
//            bufferedWriter.flush();
//            bufferedWriter.newLine();
//            bufferedWriter.close();


public class ClientHandler extends Thread {
    private Socket clientSocket;

    public ClientHandler(Socket socket) {
        super("ClientHandler");
        this.clientSocket = socket;
    }

    public void run(){
        PrintWriter serverSocketOut;
        BufferedReader serverSocketIn;
        try {
            serverSocketOut = new PrintWriter(clientSocket.getOutputStream(), true);
            serverSocketIn = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));

            String inputLine, outputLine;
            ServerProtocol serverProtocol = new ServerProtocol();
            while ((inputLine = serverSocketIn.readLine()) != null) {
                outputLine = serverProtocol.processInput(inputLine);
                serverSocketOut.println(outputLine);
                if (outputLine.equals("Bye.")) {
                    break;
                }
            }
            System.out.println("hey");
            clientSocket.close();
            serverSocketIn.close();
            serverSocketOut.close();
        } catch (IOException e) {
            System.out.println("I/O exception during execution\n");
            System.exit(1);
        }
    }
}
