import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class Client
{
	private Socket socket = null;
	private PrintWriter socketOutput = null;
	private BufferedReader socketInput = null;

	private void connect(String request, String filename) {

		try {
			// attempt to create a socket to localhost using port 8888
			socket = new Socket("localhost", 8888);

			// chain writing stream
			socketOutput = new PrintWriter(socket.getOutputStream(), true);

			// chain reading stream
			socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		} catch (UnknownHostException e) {
			System.out.println("Error: Unknown Host");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("Error: Unable to get I/O for the connection to host.\n");
			System.exit(1);
		}

		try {

			String serverMessage;
            // echo client string and write to server
            if(!request.equals("list")) {
                socketOutput.println(request);
				System.out.println("Client: " + request);
                System.out.println("Server: " + socketInput.readLine());
                socketOutput.println(filename);
				System.out.println("Client: " + filename);

			}
            else {
				System.out.println("Client: " + request);
				socketOutput.println(request);
			}
			socketOutput.println("Bye.");

			if(request.equals("list")) {
				serverMessage = socketInput.readLine();
				if(serverMessage.equals("Listing Server Files...")) {
					System.out.println("Server: " + serverMessage);
					while((serverMessage = socketInput.readLine()) != null) {
						if (serverMessage.equals("Bye.")) {
							System.out.println("Server: " + serverMessage);
							System.out.println(serverMessage);
							break;
						}
					}
				}
			}
			else if(request.equals("get")) {
				serverMessage = socketInput.readLine();
				if(serverMessage.equals("Sending File...")) {
					String fileString = null;
					System.out.println(serverMessage);
//					BufferedWriter writer = new BufferedWriter(new FileWriter("./clientFiles/" + filename));
					while((serverMessage = socketInput.readLine()) != null) {
						if(serverMessage.equals("Bye.")) {
//							writer.flush();
							byte[] byteArray = fileString.getBytes(StandardCharsets.ISO_8859_1);
							FileOutputStream fileOutputStream = new FileOutputStream("./clientFiles/" + filename);
							fileOutputStream.write(byteArray);
							System.out.println("Server: " + serverMessage);
							break;
						}
						fileString += serverMessage;
//						writer.write(serverMessage);
//						writer.newLine();
					}
//					writer.close();
				}
			}
			socketInput.close();
			socketOutput.close();
			socket.close();
        } catch (IOException e) {
            System.out.println("Error: I/O Exception during execution.\n");
            System.exit(1);
        }
	}

	public static void main( String[] args )
	{
		Client client = new Client();

		if(args[0].equals("list")) {
			client.connect("list", null);
		} else if(args[0].equals("get")) {
			client.connect("get", args[1]);
		} else if(args[0].equals("put")) {
			client.connect("put", args[1]);
		} else {
			System.out.println("\nError: Unknown client arguments");
			System.out.println("-----------------------------------------");
			System.out.println("get <filename>: request file from server");
			System.out.println("put <filename>: send a file to the server");
			System.out.println("list: lists the files on the server");
		}

	}
}